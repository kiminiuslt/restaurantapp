package app;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class AppMenu {

	int selection;
	Scanner scan;
	Scanner stockFileLoad;
	AppMenuText text;
	ResipeArray[] resipe;
	RestaurantStock[] stock;
	Order[] order;
	int stockCounter;
	int resipieCounter;
	String stockFilePath = "stock.csv";
	String resipieFilePath = "resipie.csv";
	Scanner resipieFileLoad;
	int orderCounter;

	public AppMenu() {
		super();
		initialize();
		this.selection = scan.nextInt();
		mainMenuAction(this.selection);
	}

	private void initialize() {
		this.selection = 0;
		File stockFile = new File(this.stockFilePath);
		File menuFile = new File(this.resipieFilePath);
		this.scan = new Scanner(System.in);
		try {
			this.stockFileLoad = new Scanner(stockFile);
			this.resipieFileLoad = new Scanner(menuFile);
		} catch (FileNotFoundException e) {
			System.out.println("file not found");
			e.printStackTrace();
		}
		this.text = new AppMenuText();
		this.stock = new RestaurantStock[100];
		this.resipe = new ResipeArray[100];
		this.order = new Order[100];

		for (int i = 0; i < order.length; i++) {
			this.order[i] = new Order(0, null);
		}

		for (int i = 0; i < stock.length; i++) {
			this.stock[i] = new RestaurantStock(null, 0);
		}
		for (int i = 0; i < resipe.length; i++) {

			this.resipe[i] = new ResipeArray();
		}
		loadStockDataFromCsvFile();
		loadMenuDataFromCsvFile();
	}

	private void mainMenuAction(int selection2) {

		switch (selection2) {
		case 1:
			createNewOrder(this.orderCounter);
			saveStockDataToCsvFile();
			backToMainMenu();

			break;
		case 2:
			this.text.stockManagementText();
			this.selection = this.scan.nextInt();
			stockManagementAction(this.selection);

			break;
		case 3:
			manageRestaurnatMenu();
			break;
		case 4:
			this.text.displayMenu();
			this.selection = this.scan.nextInt();
			displayAction(this.selection);
			break;

		default:
			errorBackToMainMenu();
			break;
		}
	}

	private void createNewOrder(int orderCounter) {
		int stop = 0;
		int orderedMeals = 0;
		String mealsNames = "";
		System.out.println("****** NEW ORER******");

		while (stop < 1) {
			int itemNuber = selectMenuItem();
			if (doWeHaveProducts(itemNuber)) {
				orderedMeals++;
				mealsNames = mealsNames + this.resipe[itemNuber].getMealName() + " ";
				System.out.println("Add meal? Y/N");
				if (this.scan.next().equalsIgnoreCase("n")) {
					stop = 2;

				}

			} else {
				System.out.println("not enough items for meal: " + this.resipe[itemNuber].getMealName());
				System.out.println("***** ALL ORDER CANCELED *******");
				this.order[orderCounter].setMealNames("CANCELED!");
				break;
			}

		}
		this.order[orderCounter].setMealNames(mealsNames);
		this.order[orderCounter].setMealNumber(orderedMeals);
		System.out.println("Order saved!");
		this.orderCounter++;

	}

	private boolean doWeHaveProducts(int selectMenuItem) {
		int list = this.resipe[selectMenuItem].getProductList();
		for (int i = 0; i < list; i++) {
			if (findProduct(this.resipe[selectMenuItem].getProductNames(i))) {

			} else {
				return false;
			}

		}
		return true;

	}

	private boolean findProduct(String productNames) {
		for (int i = 0; i < this.stock.length; i++) {
			if (productNames.equals(this.stock[i].getProductName())) {
				if (this.stock[i].getQuantity() > 0) {
					this.stock[i].setQuantity(this.stock[i].getQuantity() - 1);
					return true;
				}
			}
		}
		return false;

	}

	private void displayAction(int selection2) {
		switch (selection2) {
		case 1:

			printAllStockItems();
			System.out.println("Back to main menu press 1 and ENTER");
			this.scan.nextInt();
			backToMainMenu();

			break;
		case 2:
			printAllMenuItems();
			System.out.println("Back to main menu press 1 and ENTER");
			this.scan.nextInt();
			backToMainMenu();

			break;
		case 3:
			printAllOrders();
			System.out.println("Back to main menu press 1 and ENTER");
			this.scan.nextInt();
			backToMainMenu();
			break;
		case 4:
			backToMainMenu();
			break;

		default:
			errorBackToMainMenu();
			break;
		}

	}

	private void printAllOrders() {
		for (int i = 0; i < order.length; i++) {
			if (this.order[i].getMealNames() != null) {
				System.out.println((i + 1) + ". " + this.order[i].getMealNames());
			}

		}

	}

	private void printAllMenuItems() {

		for (int i = 0; i < this.resipieCounter; i++) {
			if (this.resipe[i].getMealName() != "") {
				System.out.println((i + 1) + ". " + this.resipe[i].getMealName());
				for (int j = 0; j < this.resipe[i].getProductList(); j++) {
					System.out.println("Ingridient ****  " + this.resipe[i].getProductNames(j));
				}

			}
		}

	}

	private void printAllStockItems() {
		for (int i = 0; i < this.stockCounter; i++) {
			if (this.stock[i].getProductName() != null) {
				System.out.println(
						i + 1 + ". " + this.stock[i].getProductName() + " ---> " + this.stock[i].getQuantity());
			}

		}

	}

	private void manageRestaurnatMenu() {
		this.text.manageRestaurantMenuText();
		this.selection = this.scan.nextInt();
		manageRestaurantMenuAction(this.selection);

	}

	private void manageRestaurantMenuAction(int selection2) {
		switch (selection2) {
		case 1:
			String mealName = newMealName();
			int productNumber = newResipieIngridientsNumber();
			this.resipe[this.resipieCounter].Resipe(mealName, productNumber);
			for (int i = 0; i < productNumber; i++) {
				System.out.println("Write product name and press ENTER");
				String newProduct = this.scan.next();
				this.resipe[this.resipieCounter].setProductNames(newProduct, i);

			}
			this.resipieCounter++;
			saveResipieDataToCsvFile();

			System.out.println("New resipie saved");
			backToMainMenu();

			break;
		case 2:
			editMenuItem(selectMenuItem());

			break;
		case 3:
			deleteMenuItem(selectMenuItem());
			break;
		case 4:
			backToMainMenu();
			break;

		default:
			errorBackToMainMenu();
			break;
		}

	}

	private void deleteMenuItem(int selectMenuItem) {
		this.resipe[selectMenuItem].Resipe("", 0);
		saveResipieDataToCsvFile();
		System.out.println("Menu item deleted secsefuly");
		backToMainMenu();

	}

	private void editMenuItem(int selectMenuItem) {
		System.out.println("Prevous meal name is: " + this.resipe[selectMenuItem].getMealName());
		System.out.println("Please ENTER new name");
		String newName = this.scan.next();
		System.out.println("Please enter new meal ingridients number");
		this.resipe[selectMenuItem].Resipe(newName, this.scan.nextInt());
		for (int i = 0; i < this.resipe[selectMenuItem].getProductList(); i++) {
			System.out.println("Please enter new product name and press ENTER");
			this.resipe[selectMenuItem].setProductNames(this.scan.next(), i);

		}
		saveResipieDataToCsvFile();
		System.out.println("Menu updated secsesfuly");
		backToMainMenu();

	}

	private int selectMenuItem() {
		printAllMenuItems();
		System.out.println("Please select meal number and press ENTER");
		return this.scan.nextInt() - 1;

	}

	private int newResipieIngridientsNumber() {
		System.out.println("Please write number of resipie products and press ENTER");
		return this.scan.nextInt();
	}

	private String newMealName() {
		System.out.println("Please write meal name and press ENTER");
		return this.scan.next();

	}

	private void stockManagementAction(int selection2) {
		switch (selection2) {
		case 1:
			this.text.addNewStockItem();
			String newName = this.scan.next();
			this.text.addUnitsOfItem();
			int quantityNuber = this.scan.nextInt();
			try {
				this.stock[this.stockCounter].setProductName(newName);
				this.stock[this.stockCounter].setQuantity(quantityNuber);
				this.stockCounter++;
				saveStockDataToCsvFile();
				System.out.println("item added secsesfuly");
				backToMainMenu();
			} catch (ArrayIndexOutOfBoundsException e) {

				addArrayToRestaurantStock();
				this.stock[this.stock.length] = new RestaurantStock(newName, quantityNuber);
				saveStockDataToCsvFile();
				System.out.println("item added secsesfuly");
				addArrayToRestaurantStock();
				backToMainMenu();
			}

			break;
		case 2:
			editSelectedStockItem(selectStockItem());
			saveStockDataToCsvFile();
			backToMainMenu();

			break;

		case 3:
			deleteStockItem(selectStockItem());
			saveStockDataToCsvFile();
			System.out.println("Item secsefuly deleted");
			backToMainMenu();

			break;
		case 4:
			backToMainMenu();
			break;
		default:
			errorBackToMainMenu();

			break;
		}

	}

	private void deleteStockItem(int selectStockItem) {
		this.stock[selectStockItem].setProductName(null);
	}

	private int selectStockItem() {
		printAllStockItems();
		System.out.println("Please select item number and press ENTER");
		return this.scan.nextInt() - 1;
	}

	private void editSelectedStockItem(int selectedItem) {
		System.out.println("Prevous name is: " + this.stock[selectedItem].getProductName());
		System.out.println("Please ENTER new name");
		this.stock[selectedItem].setProductName(this.scan.next());
		System.out.println("Prevous stock quantity is: " + this.stock[selectedItem].getQuantity());
		System.out.println("Please ENTER new quantity NUMBER");
		this.stock[selectedItem].setQuantity(this.scan.nextInt());
		saveStockDataToCsvFile();
		System.out.println("item updated secsesfuly");
		backToMainMenu();

	}

	private void addArrayToRestaurantStock() {
		for (int i = stock.length; i < stock.length + 5; i++) {
			this.stock[i] = new RestaurantStock("", 0);

		}

	}

	private void saveResipieDataToCsvFile() {

		FileWriter fw;
		try {
			fw = new FileWriter(this.resipieFilePath, false);
			BufferedWriter bf = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bf);
			for (int j = 0; j < this.resipieCounter; j++) {
				String ingridients = "";
				for (int i = 0; i < this.resipe[j].getProductList(); i++) {
					ingridients = ingridients + "," + this.resipe[j].getProductNames(i);

				}
				pw.println(this.resipe[j].getMealName() + "," + this.resipe[j].getProductList() + ingridients);
				pw.flush();
			}

			pw.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	private void saveStockDataToCsvFile() {

		FileWriter fw;
		try {
			fw = new FileWriter(this.stockFilePath, false);
			BufferedWriter bf = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bf);
			for (int j = 0; j < this.stockCounter; j++) {
				pw.println(this.stock[j].getProductName() + "," + this.stock[j].getQuantity());
				pw.flush();
			}

			pw.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	private void loadMenuDataFromCsvFile() {
		while (this.resipieFileLoad.hasNext()) {
			String data = resipieFileLoad.next();
			String[] values = data.split(",");
			this.resipe[this.resipieCounter].Resipe(values[0], Integer.parseInt(values[1]));
			for (int i = 0; i < Integer.parseInt(values[1]); i++) {
				this.resipe[this.resipieCounter].setProductNames(values[i + 2], i);
			}

			this.resipieCounter++;
		}
		this.resipieFileLoad.close();

	}

	private void loadStockDataFromCsvFile() {

		while (this.stockFileLoad.hasNext()) {
			String data = stockFileLoad.next();
			String[] values = data.split(",");
			this.stock[this.stockCounter].setProductName(values[0]);
			this.stock[this.stockCounter].setQuantity(Integer.parseInt(values[1]));
			this.stockCounter++;
		}
		this.stockFileLoad.close();

	}

	private void backToMainMenu() {

		this.text.mainMenu();
		this.selection = scan.nextInt();
		mainMenuAction(selection);

	}

	private void errorBackToMainMenu() {
		this.text.errorWrongSelect();
		this.text.mainMenu();
		this.selection = scan.nextInt();
		mainMenuAction(selection);
	}

}
