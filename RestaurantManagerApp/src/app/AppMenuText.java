package app;

public class AppMenuText {

	public AppMenuText() {

		mainMenu();
	}

	public void mainMenu() {
		System.out.println("********** MAIN MENU **********");
		System.out.println(" 1. New order");
		System.out.println(" 2. Stock management");
		System.out.println(" 3. Manage restaurant menu");
		System.out.println(" 4. Display");
		System.out.println(" ");
		System.out.println(" Please select menu item number and press ENTER");

	}

	public void errorWrongSelect() {
		System.out.println("************* ERROR *************");
		System.out.println("***** FALSE MENU ITEM NUMBER ****");
		System.out.println("****** PLEASE SELECT AGAIN ******");

	}

	public void stockManagementText() {
		System.out.println("********** RESTAURANT STOCK MANAGEMENT **********");
		System.out.println(" 1. Add product");
		System.out.println(" 2. Update stock");
		System.out.println(" 3. Remove stock");
		System.out.println(" 4. Back to main menu");
		System.out.println(" ");
		System.out.println(" Please select menu item number and press ENTER");

	}

	public void manageRestaurantMenuText() {
		System.out.println("********** RESTAURANT MENU MANAGEMENT **********");
		System.out.println(" 1. Add menu item");
		System.out.println(" 2. Update menu item");
		System.out.println(" 3. Remove menu item");
		System.out.println(" 4. Back to main menu");
		System.out.println(" ");
		System.out.println(" Please select menu item number and press ENTER");
	}

	public void addNewStockItem() {
		System.out.println("Please TYPE stock item name and press ENTER");
	}

	public void addUnitsOfItem() {
		System.out.println("Please TYPE new item units quantity NUMBER and press ENTER");
		
	}

	public void displayMenu() {
		System.out.println("********** DISPLAY **********");
		System.out.println(" 1. Display all stock");
		System.out.println(" 2. Display all menu item");
		System.out.println(" 3. Display all orders");
		System.out.println(" 4. Back to main menu");
		System.out.println(" ");
		System.out.println(" Please select menu item number and press ENTER");		
	}
}
