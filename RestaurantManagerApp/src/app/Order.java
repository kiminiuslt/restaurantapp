package app;

public class Order {

	private int mealNumber;
	private String mealNames;

	public int getMealNumber() {
		return mealNumber;
	}

	public void setMealNumber(int mealNumber) {
		this.mealNumber = mealNumber;
	}

	public String getMealNames() {
		return mealNames;
	}

	public void setMealNames(String mealNames) {
		this.mealNames = mealNames;
	}

	public Order(int i, String s) {
	 this.mealNumber = i;
	 this.mealNames = s;
	}

}
