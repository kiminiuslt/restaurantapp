package app;

public class RestaurantStock {

	String productName;
	int quantity;
	
	public RestaurantStock(String newName, int quantityNuber){
		
		this.productName = newName;
		this.quantity = quantityNuber;
		
		
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}
