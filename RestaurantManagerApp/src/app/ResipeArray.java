package app;

import java.util.Scanner;

public class ResipeArray {

	String mealName;
	int productList;
	String[] productNames;

	public void Resipe(String mealName, int productNumber) {
		this.productList = productNumber;
		this.productNames = new String[productNumber];
		this.mealName = mealName;
		declareStringArray(productNumber);

	}

	private void declareStringArray(int productNumber) {
		for (int i = 0; i < productNumber; i++) {
			this.productNames[i] = new String();

		}

	}

	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	public int getProductList() {
		return productList;
	}

	public void setProductList(int productList) {
		this.productList = productList;
	}

	public String getProductNames(int number) {
		return this.productNames[number];
	}

	public void setProductNames(String productNames, int number) {
		this.productNames[number] = productNames;
	}

}
